1.是什么
存储数据的容器
2.有啥用？
“快速”查找某个元素O(1), 空O（n）
3.什么时候用哈希表？
频繁的查找某一个数的时候，二分（logN）
4.怎么用哈希表
1.容器（哈希表）
2.用数组模拟简易哈希表-存储字符串中的字符，
必须在范围很小的时候用，太大效率不高

查找快。

哈希表概述
哈希表（Hash Table）是一种数据结构，它通过键值对（key-value pair）来存储数据，并使用哈希函数将键映射到表中的一个位置，以便快速查找、插入和删除操作。Java 中的 HashMap 是一种常见的哈希表实现。

哈希表的特点
高效的查找和插入：哈希表能够在平均情况下实现 O(1) 时间复杂度的查找和插入操作。
键值对存储：哈希表使用键值对的形式存储数据，每个键（key）是唯一的，可以通过键快速找到对应的值（value）。
哈希冲突处理：当不同的键通过哈希函数映射到相同的位置时，称为哈希冲突。常见的解决冲突的方法有链地址法（链表法）和开放地址法。
哈希表的用途
哈希表适用于以下场景：

快速查找：需要快速查找数据时，例如实现字典、缓存等。
去重：需要检查数据的唯一性时，例如判断集合中是否存在重复元素。
键值映射：需要通过键快速访问值时，例如实现映射关系、路由表等。
Java 中的哈希表（HashMap）
在 Java 中，HashMap 是一个常用的哈希表实现。它位于 java.util 包中，提供了高效的键值对存储和操作功能。

创建和使用 HashMap

import java.util.HashMap;

public class HashMapExample {
    public static void main(String[] args) {
        // 创建一个 HashMap
        HashMap<String, Integer> map = new HashMap<>();

        // 插入键值对
        map.put("one", 1);
        map.put("two", 2);
        map.put("three", 3);

        // 查找值
        int valueOne = map.get("one");
        System.out.println("Value for key 'one': " + valueOne);

        // 检查是否包含某个键
        boolean hasKeyTwo = map.containsKey("two");
        System.out.println("Contains key 'two': " + hasKeyTwo);

        // 删除键值对
        map.remove("two");

        // 遍历键值对
        for (String key : map.keySet()) {
            System.out.println("Key: " + key + ", Value: " + map.get(key));
        }
    }
}
HashMap 的常用方法
put(K key, V value): 插入键值对，如果键已经存在，则更新对应的值。
get(Object key): 根据键获取值，如果键不存在，则返回 null。
containsKey(Object key): 检查是否包含指定的键。
remove(Object key): 根据键删除对应的键值对。
keySet(): 返回包含所有键的 Set 视图。
values(): 返回包含所有值的 Collection 视图。
entrySet(): 返回包含所有键值对的 Set 视图。
什么时候使用哈希表
频繁查找和插入操作：当需要频繁进行查找和插入操作时，哈希表能够提供高效的性能。
数据去重：当需要检查数据的唯一性时，哈希表是一个很好的选择。
实现映射关系：当需要通过键快速访问值时，哈希表能够高效地实现映射关系。
哈希表的优缺点
优点
高效的查找和插入：平均时间复杂度为 O(1)。
灵活的键值对存储：支持任意类型的键和值。
缺点
内存消耗较大：哈希表需要额外的空间来存储哈希函数的结果和解决哈希冲突。
性能不稳定：在极端情况下（例如哈希函数不好或负载因子过高），查找和插入的时间复杂度可能退化到 O(n)。
结论
哈希表是一种高效的数据结构，适用于需要快速查找、插入和删除操作的场景。Java 中的 HashMap 提供了灵活且易用的哈希表实现，是解决许多编程问题的利器。了解哈希表的原理和使用场景，有助于在实际开发中选择合适的数据结构来提高程序性能。