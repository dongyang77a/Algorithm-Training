package 双指针;
//1.先找到最后一个复写的数:
/*双指针算法
* 1.1先判断cur位置的值
* 1.2 决定dest向后移动一步或者两步
* 1.3 判断一下dest是否已经结束为止
* 1.4 cur++
* 2.从后向前完成复写操作
* 2*/
//2.双指针-复写零 力扣1089
public class CarbonZero {
        public void duplicateZeros(int []arr){
            int cur=0,dest=-1,n=arr.length;
            //1.找到最后一个需要复写的数
            while(cur<n)
            {
                if(arr[cur]==0) dest+=2;
                else dest+=1;
                if(dest>=n-1) break;
                cur++;
            }
            //2.处理一下边界情况
            if(dest==n){
                arr[n-1]=0;
                cur--;
                dest-=2;
            }
            //3.从后往前完成复写操作
            while (cur>=0){
                if(arr[cur]!=0) arr[dest--]=arr[cur--];
                else{
                    arr[dest--]=0;
                    arr[dest--]=0;
                    cur--;
                }
            }
        }
}

