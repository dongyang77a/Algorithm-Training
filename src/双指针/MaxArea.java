package 双指针;
//盛水最多的容器力扣11题，长度为n整数数组height[n],有n条垂线，第i条垂线两个端点 坐标（i,0）(i,height[i])
//找两条垂线让他们盛水的面积最大
//思路？
//双指针， 灵感：如果中间随便选一段，两端向内逐渐推进，宽度是减少，如果长度不变总面积还是少，所以就没必要再向内收缩
// 一个max记录面积，left和right指针在两端，计算出面积，left先向右走一步，记录面积后，right向左走一步，依次，找出最大面积

public class MaxArea {
    public int maxarea(int [] height,int n){
        int left=0,right=height.length-1,max=0;


        while(left<right){
            /*if(height[left]<=height[right]){
                min=height[left];
            }else{
                min=height[right];
            }*/  //这个完全可以以下
         /*   int min=Math.min(height[left],height[right]);//最短高度*/
            int v=Math.min(height[left],height[right])*(left-right);
            max=Math.max(max,v);
            if(height[left]>height[right]){
              right--;
            }else{
                left++;
            }
        }
        return max;
    }
}
