package 双指针;

import java.util.Arrays;

/*剑指Offer 57题
* 输入一个递增排序的数组和一个数字s，在数组中查找两个数，使它们的和正好是s
* 如果有多对数字的和等于s，则输出任意一对即可,时间复杂度为O（n）
* 使用双指针算法*/
public class 和为s的两个数字 {
    public static int [] twoSum(int[] nums, int target) {
        int [] result=new int[2];
        Arrays.sort(nums);
        int left= 0,right = nums.length-1;
        while(left<right){
            if(nums[right]>=target || nums[right]+nums[left]>target){
                right--;
            }else if(nums[right]+nums[left] ==target){
                result[0]=nums[left];
                result[1]=nums[right];
                break;
            }else if(nums[right]+nums[left]<target){
                left++;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int []nums={3, 9, 12, 15};
        int target = 12;
        System.out.println(twoSum(nums,target));

    }
}
