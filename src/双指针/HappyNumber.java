package 双指针;
/*这是力扣 202.快乐数,判断n是不是一个快乐数，快乐数返回true，否则false
快乐数定义：n的每一位的平方和相加如果通过数次循环最终等于1，那么n就是快乐数，否则就不是*/
/*前提知识，通过规律表明，n通过数次循环还回回到自己循环中的数，相当于链表闭环
* 链表闭环该怎么确定？ 快慢指针呗，快指针走两步，慢指针走一步，如果是个闭环链表。快指针肯定会追上慢指针
* 因此我们可以把循环过程中的值用快慢指针来保存，当快指针追上慢指针也就是相等时结束循环然后判定是否等于1
* 1循环无数次还是1，但是非快乐数循环闭环后不等于1来进行解题*/
/*为什么想到快慢指针，1.判断链表是否存在环
    2.鸽巢原理：n个巢穴，n+1只鸽子，总会有一个巢穴有两只鸽子。如果最终结果等于1，快指针肯定会追上慢指针*/
public class HappyNumber {
        public int bitSum(int n)//返回n这个数每一位的平方和
        {
            int sum=0;
            while(n!=0)
            {
              int t=n%10;
              sum+=t*t;
              n/=10;
            }
            return sum;
        }
        public boolean isHappy(int n){
             int slow=n,fast=bitSum(n); //1.定义快慢指针
             while(slow!=fast){
                 slow=bitSum(slow);
                 fast=bitSum(bitSum(fast));//2.相当于快指针走两步
             }
             return slow==1;//3.判断相遇时的值就可
        }

}
