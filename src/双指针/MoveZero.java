package 双指针;
//双指针来进行数组的移动，时间复杂度o（N） 空间o（1）
// 将0元素移动的数组的末端，不改变非0元素的排序，不可新建数组
//分为三部分  0-dest 已处理部分   （dest+1）-（cur-1）待处理部分   cur-（length-1） 未处理部分
public class MoveZero {
    public void moveZeroes(int[] arr) {

        int length = arr.length;
        for (int cur = 0, dest = -1; cur < length; cur++) {

            if (arr[cur] != 0) {
                dest++;
                int tmp = arr[cur];
                arr[cur] = arr[dest];
                arr[dest] = tmp;

            }

        }
    }

}
