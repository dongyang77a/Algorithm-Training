package 牛客笔试训练;

import java.util.Scanner;

/*将一句话的单词进行倒置，标点不倒置。比如 I like beijing. 经过函数后变为：beijing. like I
输入描述：
每个测试输入包含1个测试用例： I like beijing. 输入用例长度不超过100
输出描述：
依次输出倒置之后的字符串,以空格分割
示例1:
输入
I like beijing.
输出
beijing. like I */
public class 倒置字符串 {



    // 注意类名必须为 Main, 不要有任何 package xxx 信息

        public static void reverse(char [] arr, int begin,int end){
            while(begin<end){
                char tmp = arr[end];
                arr[end] = arr[begin];
                arr[begin] = tmp;
                begin++;
                end--;
            }
        }

        public static void main(String[] args) {
            Scanner in = new Scanner(System.in);
            String s= in.nextLine();
            char [] arr = s.toCharArray();
            int len = arr.length;
            int begin = 0,end = len;
            reverse(arr,0,len-1);

            int i =0;
            while(i<len){
                int j = i;
                while(j < len && arr[j] != ' '){

                    j++;

                }
                if(j<len){
                    reverse(arr,i,j-1);
                    i = j+1;
                }else{
                    reverse(arr,i,j-1);
                    i= j;

                }
            }
            String  str = new String(arr);
            System.out.println(str);
        }
    }

