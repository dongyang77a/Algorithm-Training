package 牛客笔试训练;

import java.util.Scanner;

/*链接：https://www.nowcoder.com/questionTerminal/2d3f6ddd82da445d804c95db22dcc471?answerType=1&f=discussion
来源：牛客网

牛牛定义排序子序列为一个数组中一段连续的子序列,并且这段子序列是非递增或者非递减排序的。牛牛有一个长度为n的整数数组A,他现在有一个任务是把数组A分为若干段排序子序列,牛牛想知道他最少可以把这个数组分为几段排序子序列.
如样例所示,牛牛可以把数组A划分为[1,2,3]和[2,2,1]两个排序子序列,至少需要划分为2个排序子序列,所以输出2

输入描述:
输入的第一行为一个正整数n(1 ≤ n ≤ 10^5)

第二行包括n个整数A_i(1 ≤ A_i ≤ 10^9),表示数组A的每个数字。


输出描述:
输出一个整数表示牛牛可以将A最少划分为多少段排序子序列
示例1
输入
6
1 2 3 2 2 1
输出
2*/
public class 排序子序列 {


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextInt()) {
            int n = in.nextInt();
            int[] arr = new int[n + 1];
            //数组大小为n+1是为了防止下面数组越界。
            for (int i = 0; i < n; i++) {
                //判断条件不能是arr.length;
                arr[i] = in.nextInt();
            }
            int i = 0;
            int count = 0;
            while (i < n) {
                if (arr[i] < arr[i + 1]) {
                    //表示该子序列非递增
                    while (i < n && arr[i] < arr[i + 1]) {
                        //判断下一个数字是否可以划分到这个子序列
                        i++;
                    }
                    count++; //表示该序列完毕，用count记录当前有几个子序列
                    i++;
                } else if (arr[i] == arr[i + 1]) {
                    //相等i+1跳过。
                    i++;
                } else {
                    //非递减序列
                    while (i < n && arr[i] > arr[i + 1]) {
                        i++;
                    }
                    count++;
                    i++;
                }
            }
            System.out.println(count);
        }
    }

}
