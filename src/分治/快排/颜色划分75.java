package 分治.快排;
// 将数组分为三块， left在 值为 0 块的最右边，right在2块的最左边，i在中间
//这样 [0,left] 都为 0， [left+1,（i-1)]都为1，[i,right-1]待扫描，[right,n-1]全为2

public class 颜色划分75 {
    public void swap(int [] nums,int i,int j)
    {
        int t = nums[i];
        nums[i] = nums[j];
        nums[j] = t;
    }
    public void sortColors(int [] nums){
        int left = -1,right = nums.length,i = 0;

        while(i< right)
        {
            if(nums[i] == 0) swap(nums,++left,i++);
            else if(nums[i] == 2) swap(nums,--right,i);
            else i++;
        }
    }
}
