package BFS解决FloodFill算法.多源;

import java.util.LinkedList;
import java.util.Queue;

public class 地图中的最高点 {



    public static int [][] highestPeak(int[][] isWater) {
        int [] dx = {0,0,1,-1};
        int [] dy = {1,-1,0,0};
        int m = isWater.length;
        int n = isWater[0].length;
        int [][] dist = new int[m][n];
        boolean [][] visted = new boolean[m][n];
        Queue<int []> q = new LinkedList<>();
        for(int i = 0;i<m;i++)
        {
            for(int j = 0;j<n;j++)
            {
                if(isWater[i][j] == 1)
                {
                    visted[i][j] = true;
                    dist[i][j] = 0;
                    q.add(new int[]{i,j});
                }
            }
        }
        while(!q.isEmpty())
        {
            int [] cur = q.poll();
            int a = cur[0],b = cur[1];
            for(int i = 0;i<4;i++)
            {
                int x = a + dx[i];
                int y = b + dy[i];
                if(x>=0&&x<m && y>=0&&y<n && !visted[x][y])
                {
                        dist[x][y] = dist[a][b]+1;
                        q.add(new int[]{x,y});
                        visted[x][y] = true;
                }
            }
        }

        for(int i = 0;i<m;i++)
            for(int j=0;j<n;j++)
            {
                if(dist[i][j] == 1)
                {
                    q.add(new int[]{i,j});
                }
            }

            return dist;

    }


    public static void main(String[] args) {
        int [][] isWater = {{0,0,1},{1,0,0},{0,0,0}};

        System.out.println(highestPeak(isWater));
    }
}
