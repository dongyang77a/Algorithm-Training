package BFS解决FloodFill算法.多源;

import java.util.LinkedList;
import java.util.Queue;

public class 多源最短路问题 {

    int [] dx = {0,0,1,-1};
    int [] dy = {1,-1,0,0};
    public int[][] minDistance(int[][] grid) {
        int m = grid.length,n = grid[0].length;
        int [][] dist = new int[m][n];
        boolean [][] vis = new boolean[m][n];
        Queue<int[]> q = new LinkedList<>();
        for(int i = 0;i < m;i++){
            for(int j = 0;j < n;j++){
                if(grid[i][j] == 0)
                {
                    dist[i][j] = 0;
                    q.add(new int[]{i,j});
                }
               else{
                   dist[i][j] = -1;
                }
            }
        }
        while(!q.isEmpty())
        {
            int [] cur = q.poll();
            int a = cur[0],b = cur[1];

            for(int i = 0 ;i<4;i++)
            {
                int x = a + dx[i];
                int y = b + dy[i];
                if(x>0 && x<m && y>0 && y<n && dist[x][y] == -1)
                {
                    dist[x][y] = dist[a][b] + 1;
                    q.add(new int[]{x,y});
                }

            }
        }
        return  dist;
    }
}
