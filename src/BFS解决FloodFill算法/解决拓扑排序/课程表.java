package BFS解决FloodFill算法.解决拓扑排序;

import java.util.*;
// Map(1,(2,3))  意思是 2->1, 3->1 ，排序
public class 课程表 {

    public boolean canFinish(int numCourses, int[][] prerequisites) {
        //1.准备工作
        int [] in = new int[numCourses];//统计每一个点的入度
        Map<Integer, List<Integer>> edges = new HashMap<>();//邻接表存图

        //2.建构图
        for(int i = 0;i<prerequisites.length;i++)
        {
            int a = prerequisites[i][0],b = prerequisites[i][1];
            if(!edges.containsKey(b))
            {
                edges.put(b,new ArrayList<>());
            }
            edges.get(b).add(a);
            in[a]++;
        }

        Queue<Integer> q = new LinkedList<>();
        // 1.先把入度为 0 的点，加入到队列中
        for(int i = 0;i<numCourses;i++)
        {
            if(in[i] == 0)
            {
                q.add(i);
            }
        }
        //2. bfs
        while(!q.isEmpty())
        {
            int cur = q.poll();
           for(int a :edges.getOrDefault(cur,new ArrayList<>() ))
           {
               in[a]--;
               if(in[a] == 0) q.add(a);
           }
        }

        //4.判断是否有环
        for(int x : in)
        {
            if(x !=0)
            {
                return false;
            }
        }
        return true;
    }
}
