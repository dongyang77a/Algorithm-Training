package 位运算;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class 只出现一次的数字 {
    public static  int singleNumber(int[] nums) {
        int ans = nums[0];

        if(nums.length>1){
            for(int i = 1;i<nums.length ; i++){
                ans = ans^nums[i];
            }
        }
        return ans;
    }

    public int singleNumber2(int[] nums) {
        Map<Integer,Integer> map = new HashMap<>();
        for(Integer i :nums){
            Integer count = map.get(i);
            count = count  == null ? 1:++count;
            map.put(i,count);
        }
        for(Integer i : map.keySet()){
            Integer count = map.get(i);
            if(count == 1){
                return i;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
       int [] nums = {4,1,2,1,2};
       int result= singleNumber(nums);
        System.out.println(result);
    }
}
