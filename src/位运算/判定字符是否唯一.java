package 位运算;

import java.util.Arrays;
//https://leetcode.cn/problems/is-unique-lcci/
public class 判定字符是否唯一 {

    public boolean isUnique2(String astr) {

        // 利用鸠巢原理来优化
        if(astr.length()> 26){
            return false;
        }
        int bitMap = 0;
        for(int i  =0;i<astr.length();i++) {
            int x = astr.charAt(i) - 'a';
           /*在ASCII编码中，小写字母 'a' 到 'z' 连续排列，其对应的ASCII码值依次为 97 到 122。
           这意味着，假设 astr 中只包含小写字母，可以通过将字符的ASCII码值减去小写字母 'a' 的ASCII码值来获取与字母表对应的索引。*/

            //先判断字符是否在位图中
            if (((bitMap >> x) & 1) == 1) return false;
            /*具体到这段代码中的应用，我们需要判断字符是否在位图中。
            通过右移 x 位后，我们可以将位图中的第 x 位（从右往左数）移动到最右边的位置，
            然后通过与 1 进行按位与操作（& 1），来获取最右边的位是否为 1。
         如果最右边的位为 1，表示该字符在位图中已经存在，说明字符重复。如果最右边的位为 0，表示该字符在位图中不存在，字符不重复。*/

            // 把当前字符加入到位图中
            bitMap |= 1 << x;
            //bitMap |= 1 << x 的效果是将位图 bitMap 中的第 x 位设置为 1，保持其他位不变。

        }
        return true;


    }
    public boolean isUnique(String astr) {
        char[] hash = new char[128];
        char[] Array = astr.toCharArray();
        for (int i = 0; i < Array.length; i++) {
            if (hash[Array[i]] != 0) return false;
            else hash[Array[i]]++;

        }

        return true;

    }
}