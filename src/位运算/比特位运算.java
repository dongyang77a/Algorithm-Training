package 位运算;
/*https://leetcode.cn/problems/counting-bits/submissions/521893679/*/
import java.util.Scanner;

public class 比特位运算 {
    public static int[] countBits(int n) {
        int [] bits = new int [n+1];
        for(int i = 0 ;i<=n;i++){
            bits[i] = countOnes(i);
        }
        return bits;
    }
    public static int countOnes(int x){
        int ones = 0;
        while(x>0){
            x&= (x-1);
            ones++;
        }
        return ones;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n  = in.nextInt();

        System.out.println(countBits(n));
    }
}
