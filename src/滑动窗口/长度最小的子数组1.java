package 滑动窗口;
// 滑动窗口就是 利用单调性，让同向双指针都不回退的情况下依次前进，
// 像组成一个窗口进行向前滑动一样
public class 长度最小的子数组1 {
    public static int findMin(int[] nums, int target){

        int result = Integer.MAX_VALUE;
        int sum=0;
        for( int left = 0,right = 0; right< nums.length;right++){
            sum+=nums[right];//进窗口
            while(sum>=target){
                result=Math.min(result,right-left+1);
                sum-=nums[left++];//出窗口
            }
        }
        return result == Integer.MAX_VALUE ? 0:result;
    }

    public static void main(String[] args) {
        int []nums = {2,3,1,2,4,3};
        int target = 7;
        System.out.println( findMin(nums,target));


    }

}
/*给定一个含有 n 个正整数的数组和一个正整数 target 。

找出该数组中满足其总和大于等于 target 的长度最小的 连续子数组 [numsl, numsl+1, ..., numsr-1, numsr] ，并返回其长度。如果不存在符合条件的子数组，返回 0 。
示例 1：

输入：target = 7, nums = [2,3,1,2,4,3]
输出：2
解释：子数组 [4,3] 是该条件下的长度最小的子数组。
示例 2：

输入：target = 4, nums = [1,4,4]
输出：1
示例 3：

输入：target = 11, nums = [1,1,1,1,1,1,1,1]
输出：0
 */