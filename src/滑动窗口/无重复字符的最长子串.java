package 滑动窗口;

public class 无重复字符的最长子串 {
  public static  int findMax(String ss){

      char [] s= ss.toCharArray();

      int [] hash = new int [128]; //数组模拟哈希表
      int left=0,right=0,n=ss.length();
      int ret = 0;
      while(right<n){
          hash[s[right]]++; // 进窗口
          while (hash[s[right]]>1){
              hash[s[left++]]--; //出窗口
          }
          ret = Math.max(ret,right-left+1); //更新结果
          right ++; //让下一个字符进入窗口

      }

        return ret;
      }


    public static void main(String[] args) {
        String ss ="abcabcbb";
      //  System.out.println(findMax(ss));



    }
  }


