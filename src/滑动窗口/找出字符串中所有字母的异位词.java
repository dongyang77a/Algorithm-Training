package 滑动窗口;

import javax.xml.transform.Result;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class 找出字符串中所有字母的异位词 {

    public List<Integer> findAnagrams(String ss, String pp) {
        char [] s = ss.toCharArray();
        char [] p =pp.toCharArray();
        int size = s.length;
        int max = p.length;
        if(s.length<p.length){
            return  new ArrayList<Integer>();
        }
        int [] hash1 = new int[128];
        int [] hash2 = new int [128];

        List<Integer> ans = new ArrayList<Integer>();
        for( char x : p){
            hash2[x]++;
        }

        for(int left = 0,right = 0;right<size;right++)
        {
            char in = s[right];
            if(right-left+1>max)
            {
                char out = s[left];
                hash1[out]--;  //出字符
                left++;
            }

            hash1[in]++; //进字符


            if(Arrays.equals(hash1,hash2)){
                ans.add(left);
            }



        }
        return ans;

    }





    public static void main(String[] args) {
        String s = "cbaebabacd";
        String p = "abc";




    }
}
