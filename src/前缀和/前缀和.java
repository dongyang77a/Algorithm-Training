package 前缀和;

import java.util.Scanner;

/*给定一个长度为 n 的数组， 卡接下来有 q次查询， 每次查询有 两个参数，L,R。输出AL+...+AR的和
* 输出q行。每行代表一次查询的结果
* 3 2 数组中有三个数，有两次查询
* 1 2 4  数组
* 1 2  查询 1 到 2 的和
* 2 3  */
public class 前缀和 {
    public static void main(String[] args) {
        Scanner in  =  new Scanner(System.in);
        // 1、读入数据
        int n = in.nextInt(),q = in.nextInt();
        int [] arr = new int [n+1];
        for(int i = 1;i<=n;i++){ //下标从1开始便于处理细节问题
            arr [i] = in.nextInt();
        }
        //2. 预处理一个前缀与数组
        long [] dp = new long[n+1];
        for(int i = 1 ;i<=n;i++){
            dp[i]=dp[i-1]+arr[i];
        }
        //3. 使用前缀和数组  L-R
        while(q>0){
            int L = in.nextInt(),R=  in.nextInt();
            System.out.println(dp[R]-dp[L-1]);
            q--;
        }
    }
}
