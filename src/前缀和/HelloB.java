package 前缀和;
class HelloA {


    public HelloA() {
        System.out.println("I m A class");
    }

    static {
        System.out.println("static A");
    }
}
public class HelloB extends  HelloA{
    public HelloB(){
        System.out.println("i am b class");
    }
     static {
         System.out.println("static b");
     }

    public static void main(String[] args) {
        new HelloB();
    }
}

