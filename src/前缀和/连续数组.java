package 前缀和;
/*https://leetcode.cn/problems/contiguous-array/submissions/521524506/*/
import java.util.HashMap;
import java.util.Map;
/*优化思路： 把 0 当成 -1 ，这样和为 0时，说明有成对的 0 1 */
public class 连续数组 {
    public static int findMaxLength(int[] nums){
        Map<Integer,Integer> hash = new HashMap<Integer,Integer>();
         hash.put(0,-1);// 就是为了进入 hash.containsKey(sum)  如果没有该语句则 [0,1] 无法进入if判断句

        int sum = 0,ret = 0;

        for(int i = 0;i< nums.length;i++){
            sum+=(nums[i] == 0 ? -1:1);
            if(hash.containsKey(sum)) ret = Math.max(ret,i-hash.get(sum));
            else hash.put(sum,i);
        }
        return ret;

    }

    public static void main(String[] args) {
        int [] nums = {0,1};
        findMaxLength(nums);

    }
}
