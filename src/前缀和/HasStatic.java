package 前缀和;

public class HasStatic {
    private static int x = 100;
    public static void main(String[] args) {
        HasStatic hs1 = new HasStatic();
        hs1.x++;
        HasStatic hs2 = new HasStatic();
        hs2.x++;
        hs1 = new HasStatic();
        hs1.x++;
        HasStatic.x--;
        System.out.println("x ="+x);
    }
}


/*在这个代码中，x 是一个静态变量（static int x = 100;），它属于 HasStatic 类本身而不是实例化的对象。

当你创建一个类的实例对象时，每个对象都有自己的实例变量。但是，静态变量属于类本身，被所有实例对象所共享。这意味着当你修改一个对象的静态变量时，其他对象也会受到影响。

在代码中，首先创建了 hs1 对象，并将 x 的值增加1。然后创建了另一个 hs2 对象，并再次将 x 的值增加1。此时，x 的值变为102。

接着，又创建了一个新的 hs1 对象，并将 x 的值增加1。此时，x 的值变为103。

然后，通过 HasStatic.x-- 将 x 的值减少1。此时，x 的值变为102。

最后，通过 System.out.println("x ="+x); 输出 x 的值，结果为102。

所以，虽然 x 是静态变量，但由于它被多个对象共享，对它的修改会影响所有对象*/