package 队列宽搜;

import java.util.*;

public class 找到每一层最大的值 {
    public List<Integer> largestValues(TreeNode root)
    {
        List<Integer> list = new ArrayList<>();

        Queue<TreeNode> q = new LinkedList<>();
        q.add(root);
        while(!q.isEmpty())
        {
            int size = q.size();
            int tmp = Integer.MIN_VALUE;
            for(int i = 0 ;i<size;i++)
            {
                TreeNode poll = q.poll();
                tmp = Math.max(tmp,poll.val);
                if(poll.left!=null)
                {
                    q.add(poll.left);
                }
                if(poll.right!=null)
                {
                    q.add(poll.right);
                }
            }
            list.add(tmp);
        }
        return list;
    }
}
