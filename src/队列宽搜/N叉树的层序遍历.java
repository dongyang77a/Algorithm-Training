//package 队列宽搜;
//
//import javax.xml.soap.Node;
//import java.util.ArrayList;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Queue;
//
//// 本题采用bfs 广度优先遍历，先把每层的节点遍历出来，然后在这层节点出队列的时候添加该节点的孩子节点
//// 如果孩子节点为空就遍历下一个节点
//public class N叉树的层序遍历 {
//
//
//
//    public List<List<Integer>> levelOrder(Node root)
//    {
//        List<List<Integer>> ret = new ArrayList<>();
//        if(root == null) return ret;
//
//        Queue<Node> q = new LinkedList<>();
//        q.add(root);
//
//        while(!q.isEmpty())
//        {
//            int size = q.size();
//            List<Integer> tmp = new ArrayList<>();//统计本层的绩点信息
//            for(int i = 0 ;i<size;i++)
//            {
//                Node t = q.poll();
//                tmp.add(t.val);
//                for(Node child : t.children)
//                {
//                    if(child !=null)
//                    {
//                        q.add(child);
//                    }
//                }
//            }
//            ret.add(tmp);
//        }
//        return ret;
//    }
//}
