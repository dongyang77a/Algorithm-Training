//package 队列宽搜;
//
//import 牛客101.链表.ListNode;
//// 自己做一个标识来判定是否要逆序，如果逆序就用这个函数
//import javax.swing.tree.TreeNode;
//import java.util.*;
//
//public class 二叉树的锯齿形层序遍历 {
//    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
//        List<List<Integer>> ret = new ArrayList<>();
//        if(root == null) return  ret;
//        Queue<TreeNode>  q = new LinkedList<>();
//        q.add(root);
//        int level = 1;
//        while(!q.isEmpty())
//        {
//            int size = q.size();
//            List<Integer> tmp = new ArrayList<>();
//            for(int i = 0 ; i< size ;i++)
//            {
//                TreeNode t = q.poll();
//                tmp.add(t.val);
//                if(t.left !=null) q.add(t.left);
//                if(t.right != null) q.add(t.right);
//
//            }
//            // 判断是否逆序
//            if(level%2 == 0)
//            {
//                Collections.reverse(tmp);
//            }
//            ret.add(tmp);
//            level++;
//        }
//         return ret;
//    }
//}
