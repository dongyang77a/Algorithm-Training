package 优先级队列;

import java.util.PriorityQueue;

public class 最后一块石头的重量 {

    public int lastStoneWeight(int[] stones) {
        // 1. 创建一个大根堆
        PriorityQueue<Integer> heap = new PriorityQueue<>((a, b) -> b - a);
        // 2.把所有的石头放到堆里
        for (int x : stones) {
            heap.offer(x);
        }
        //3.模拟
        while (heap.size() > 1) {
            int a = heap.poll();
            int b = heap.poll();
            if (a > b) {
                heap.offer(a - b);
            }
        }
        return heap.isEmpty() ? 0 : heap.poll();
    }

    // 1. 创建一个大根堆
    private void buildMaxHeap(int [] arr)
    {
        for(int i = (arr.length-2)/2; i>=0; i--)
        {
            maxHeapify(arr,i,arr.length);
        }
    }
    private void maxHeapify(int [] arr , int index , int size)
    {
        int largest = index ;
        int left = 2*index +1;

    }

}
