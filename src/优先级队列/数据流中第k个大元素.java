package 优先级队列;

import com.sun.javafx.scene.control.skin.NestedTableColumnHeader;

import java.util.PriorityQueue;

public class 数据流中第k个大元素 {
    // 创建一个大小为k的小根堆
    PriorityQueue<Integer> heap;
    int _k;

    public void KthLargest(int k, int[] nums)
    {
        _k = k;
        heap = new PriorityQueue<>();
        for(int x : nums)
        {
            heap.offer(x);
            if(heap.size()>_k)
            {
                heap.poll();
            }
        }
    }
    public int add(int val)
    {
        heap.offer(val);
        if(heap.size()>_k)
        {
            heap.poll();
        }
        return heap.peek();
    }
}
