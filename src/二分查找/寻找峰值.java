package 二分查找;
/*https://leetcode.cn/problems/find-peak-element/description/*/
public class 寻找峰值 {


    public int findPeakElement2(int[] nums) {
        // 当mid>mid+1 时说明是在走下坡路，左部分肯定有峰值，但是右部分不一定有，因为可能右部分是一直下降的
        int  left = 0 , right = nums.length - 1;
        while(left<right){
            int mid = left+(right-left)/2;
            if(nums[mid]>nums[mid+1]){
                right = mid;
            }else {
                left= mid+1;
            }

        }
        return left;
    }
    public int findPeakElement(int[] nums) {
        int  left = 0 , right = nums.length - 1;
        while(left<right){
            int mid = left+(right-left+1)/2;
            if( nums[mid]>nums[mid-1]){
                left  =  mid;

            }else{
                right = mid-1;
            }

        }
        return left;
    }
}
