package 二分查找;
/*二段性也可以用二分查找算法
* https://leetcode.cn/problems/B1IidL/*/
import java.util.Arrays;
// 一个大问题，当 新建 int [] game = arr 时，并不是创建了新的数组，而是game的内存地址指向了arr，修改game，arr也会变化

/*在Java中，当使用 int [] game = arr; 这样的语句时，
确实是将 game 的引用指向了 arr 所指向的数组，而没有创建一个新的数组。
因此，如果修改了 game 中的元素，那么 arr 中对应位置的元素也会相应地被修改。
这是因为它们实际上是指向同一个数组的不同引用。如果想要创建一个新的数组，
可以使用 int[] game = Arrays.copyOf(arr, arr.length); 这样的语句。*/
public class 山脉数组的峰顶索引 {
    public static int peakIndexInMountainArray(int[] arr) {
        int [] game = new int [arr.length];
        int z = 0;
        for( int x : arr){

            game[z++] = x;
        }
        Arrays.sort(game); // 排序从小到大
        int max = game[game.length-1];

        for( int left = 0;left<arr.length-1;left++){
            if(arr[left]==max){
                return  left;
            }
        }
        return -1;
    }


    public int peakIndexInMountainArray2(int[] arr) {

        int left = 0 , right =arr.length-2;//小优化，因为队尾那个数用不着
        while(left<right){
            int mid = left+(right-left+1)/2;

            if(arr[mid]>arr[mid-1]){
                left = mid;
            }else {
                right = mid-1;
            }
        }
        return left;
    }

    public static void main(String[] args) {
        int [] arr = {0,1,0};
      int result =  peakIndexInMountainArray( arr);
        System.out.println();
    }

}
