package 二分查找;
/*https://leetcode.cn/problems/find-minimum-in-rotated-sorted-array/description/*/
public class 寻找旋转排序数组中最小值 {
    /*根据数组的变化值可以发现，他的趋势都是递增，选择最右边的点为参照点进行划分区域
    * x的值可以不变，根据mid的值逐渐变化left 与 right 的位置逐渐靠近从而锁定*/
    public int findMin(int[] nums) {

        int left = 0, right = nums.length - 1;
        int x = nums[right];
        while (left < right) {
            int mid = left + (right - left) / 2;

            if (nums[mid] > x) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        return nums[left];
    }

    public static void main(String[] args) {
        int[] nums = {3,4,5,1,2};
        System.out.println(new 寻找旋转排序数组中最小值().findMin(nums));
    }
}
