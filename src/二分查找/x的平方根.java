package 二分查找;
/*给定一个非负整数 x ，计算并返回 x 的平方根，即实现 int sqrt(int x) 函数。
正数的平方根有两个，只输出其中的正数平方根。
如果平方根不是整数，输出只保留整数的部分，小数部分将被舍去。
示例 1:
输入: x = 4
输出: 2
示例 2:
输入: x = 8
输出: 2
解释: 8 的平方根是 2.82842...，由于小数部分将被舍去，所以返回 2*/
 //
public class x的平方根 {
    public int mySqrt(int x) {
        if(x<1) return 0;
        long left = 1,right = 9;
        while(left<right){//严谨范围
            long mid = left + (right-left+1)/2;
            if(mid*mid > x) right = mid-1;   // 找到最佳的位置以免错过x的平方根
            else  left = mid;

        }
        return  (int)left;
    }
}
