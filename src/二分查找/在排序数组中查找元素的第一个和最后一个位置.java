package 二分查找;

public class 在排序数组中查找元素的第一个和最后一个位置 {
    public int[] searchRange(int[] nums, int target) {

        // mid在左端点时，就是找到最左的端点此时就要注意left的位置不能超过第一个点，所以 num[mid]<target,left=mid+1;
        int left = 0,right = nums.length-1;
        int [] result = new int [2];
        result[0]=result[1]=-1;
        if(nums.length == 0) return result;
        while (left<right){
            int mid = (right-left)/2+left;
            if(nums[mid]<target) left = mid+1;
            else right = mid;
        }
        if(nums[left]!=target){
            return result;
        }else{
            result[0] = left;
        }

        // mid 在右端点
        // mid在右端点时，就是找到最左的端点此时就要注意right的位置不能错过最后一个点，left随意，所以 num[mid]>target,right=mid-1;
        left = 0;
        right= nums.length-1;
        while(left<right){
            int mid = (right-left+1)/2+left;
            if(nums[mid]<=target) left = mid ;
            else right = mid-1;
        }

        result[1]=left;

        return result;


    }
}
