package 模拟;

public class N字形变换 {
    public String convert(String s,int numRows)
    {
        int d =2*numRows-2,n=s.length();
        StringBuilder ret = new StringBuilder();
        //1.处理第一行
        for(int i=0;i<n;i+=d)
            ret.append(s.charAt(i));
        //2.处理中间行
        for(int k = 1;k<numRows-1;k++)//依次枚举中间行
        {
            for(int i = k,j=d-1;i<n||j<n;i+=d,j+=d)
            {
                if(i<n) ret.append(s.charAt(i));
                if(j<n) ret.append(s.charAt(j));

            }
        }
        //3.处理最后一行
        for(int i = numRows-1;i<n;i+=d)
            ret.append(s.charAt(i));
        return ret.toString();
    }


}
