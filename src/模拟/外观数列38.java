package 模拟;
//该问题内容是解释型号
// 比如 1：1 2：11 // 解释了第一个数中有一个1，并且相同且相邻来计数

// 3：21  //解释2：11中有2个1
// 4: 1211  解释3 中有一个2 一个 1

public class 外观数列38 {
    public String countAndSay(int n){
        String ret = "1"; //初始数字
        for(int i = 1;i<n;i++) //解释n-1 次ret
        {
            StringBuilder tmp = new StringBuilder();
            int len = ret.length(); //
            for(int left = 0,right = 0;right<len;)//双指针算法来检测
            {
                 while (right < len && ret.charAt(left) == ret.charAt(right)) right++; //进行划分个数,相同的进行count算入
                 tmp.append(Integer.toString(right-left));//个数
                 tmp.append(ret.charAt(left));//元素名
                 left = right; //进行 下一个查找

            }
            ret = tmp.toString(); //更新ret

        }
        return ret;
    }
}
