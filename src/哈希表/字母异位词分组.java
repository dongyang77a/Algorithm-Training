package 哈希表;

import java.util.*;

/*给一个字符数组，将字母异位词组合在一起，可以按任意顺序返回结果列表
* */
public class 字母异位词分组 {

    public List<List<String>> groupAnagrams(String [] strs)
    {
        Map<String,List<String>> hash = new HashMap<>();

        //1.先把所有的字母异位词分组
        for(String s :strs){
            char[] tmp = s.toCharArray();
            Arrays.sort(tmp);
            String key = new String(tmp);

            if(!hash.containsKey(key)){
                hash.put(key,new ArrayList<>());
            }
            hash.get(key).add(s);
        }
        //2. 提取结果
        return new ArrayList(hash.values());
    }
}
