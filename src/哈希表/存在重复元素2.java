package 哈希表;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
// 给个整数数组，如果存在重复元素且元素下标距离的绝对值<=k 就返回 true
//否则就false
public class 存在重复元素2 {
    public boolean solution(int [] nums,int k){
        int len =nums.length;
        if(len == 0){
            return false;
        }

        HashMap<Integer,Integer> hash = new HashMap<>();
        for(int i = 0;i<len;i++){
            if(hash.containsKey(nums[i])){
                if( Math.abs(hash.get(nums[i])-i)<=k){
                    return  true;
                }

            }
            hash.put(nums[i],i);
        }
        return false;
    }
}
