package 哈希表;

import java.util.HashMap;
import java.util.Map;

//
public class 两数之和 {
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> hash = new HashMap<>();//<nums[i],i>
        for (int i = 0; i < nums.length; i++) {
            int x = target - nums[i];
            if (hash.containsKey(x)) {
                return new int[]{i, hash.get(x)};
            }
            hash.put(nums[i], i);
        }

        return null;
    }
}
