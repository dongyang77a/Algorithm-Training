package 哈希表;

import java.util.HashMap;

public class 判定是否互为字符重排 {

    public boolean CheckPermutation(String s1, String s2) {
        if(s1.length()!= s2.length()){
            return false;
        }
        int [] hash = new int[26];
        // 先把第一个字符串的信息统计到哈希表中
        for(int i = 0 ;i<s1.length();i++){
            hash[s1.charAt(i)-'a']++;
        }

        // 再遍历第二个字符串，判断是否可以重排
        for(int i = 0;i<s2.length();i++){
            hash[s2.charAt(i)-'a']--;
            if(hash[s2.charAt(i)-'a']<0){
                return false;
            }
        }

        return true;
    }
}
