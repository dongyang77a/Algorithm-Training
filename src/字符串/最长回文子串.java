package 字符串;
// 中心拓展算法
//1.固定中心点 2.从中心点开始，向两边扩展
// 注意：奇数长度以及偶数长度都需要考虑
public class 最长回文子串 {
    public String longestPalindrome(String s)
    {
        int begin = 0,len = 0, n = s.length();
       // 先固定中间点
        for(int i = 0 ; i< n ;i++)
        {
            // 先拓展 奇数长度的子串
            int left = i,right = i;
            while(left>=0&&i<n&& s.charAt(left) == s.charAt(right)){
                left--;
                right++;
            }
            if(right-left-1>len){
                begin = left+1;
                len = right-left-1;
            }
            // 拓展偶数
            left = i;
            right = i+1;
            while(left>=0&& right<n && s.charAt(left) == s.charAt(right))
            {
                left--;
                right++;
            }
            if(right-left-1>len)
            {
                begin = left+1;
                len = right-left-1;
            }

        }
        return s.substring(begin,begin+len);
    }

}
