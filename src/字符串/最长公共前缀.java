package 字符串;

public class 最长公共前缀 {

    public String longestCommonPrefix(String [] strs)
    {
        // 解法一：两两比较
        String ret = strs[0];
        for(int i = 1; i<strs.length;i++)
        {
            ret  = findCommon(strs[i],ret);
        }
        return ret;
    }
    public String findCommon(String s1,String s2)
    {
        int i =0;
        int len = Math.min(s1.length(),s2.length());
        while( i<len &&  s1.charAt(i) == s2.charAt(i))
            i++;

        return s1.substring(0,i);
    }

    // 统一比较

    public  String NumTwo(String [] args){

        for(int i = 0 ; i<args[0].length();i++)
        {
            char tmp = args[0].charAt(i);
            for(int j = 1 ; j<args.length;j++)
            {
                if( i == args[j].length() || args[j].charAt(i) != tmp)
                    return args[0].substring(0,i);
            }
        }
        return  args[0];
    }
}
