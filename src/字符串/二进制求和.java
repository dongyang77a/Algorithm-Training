package 字符串;

public class 二进制求和 {

    public String addBinary(String a ,String b)
    {
        StringBuffer  ret = new StringBuffer();
        int cur1 = a.length()-1, cur2 = b.length()-1,t = 0;
        while(cur1>=0 || cur2>= 0 || t!=0)
        {
            if(cur1>=0) t+=a.charAt(cur1--) - '0';
            if(cur2>=0) t+= b.charAt(cur2--)-'0';
            ret.append((char)('0'+(char)(t%2)));
            t/=2;
        }
        ret.reverse();
        return ret.toString();
    }
}
