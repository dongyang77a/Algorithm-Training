package 字符串;
/*给定两个以字符串形式表示的非负整数 num1 和 num2，返回 num1 和 num2 的乘积，它们的乘积也表示为字符串形式。

注意：不能使用任何内置的 BigInteger 库或直接将输入转换为整数。*/
public class 字符串相乘 {

    public String multiply(String a,String b)
    {
        if("0".equals(a) || "0".equals(b))
        {
            return "0";
        }
        int len1 = a.length();
        int len2 = b.length();
        int [] ans = new int [len1+len2];

        for(int i = len1-1;i>=0;i--)
        {
            int x = a.charAt(i)-'0';
            for(int j = len2-1;j>=0;j--)
            {
                int y = b.charAt(j)-'0';
                int sum = ans[i+j+1] + x*y;
                ans[i+j+1] = sum%10;
                ans[i+j] +=sum/10;
            }
        }
        StringBuffer sb = new StringBuffer();
        for(int i = 0 ;i<ans.length;i++)
        {
            if(i == 0 && ans[i]==0)
            {
                continue;
            }
            sb.append(ans[i]);
        }
        return sb.toString();
    }
}
