package 牛客101.链表;

public class 合并两个排序的链表 {

    public ListNode Merge (ListNode pHead1, ListNode pHead2) {
        // write code here
        ListNode head = new ListNode(-1);
        ListNode current = head;
        ListNode num1 = pHead1;
        ListNode num2 = pHead2;

        while(num1!=null && num2!=null)
        {
            if(num1.val>= num2.val)
            {
                current.next = num2;
                num2 = num2.next;
            }else{
                current.next = num1;
                num1 = num1.next;
            }
            current = current.next;
        }
       if(num1!=null)
       {
           current.next =  num1;
       }
       if(num2!=null)
       {
           current.next = num2;
       }

       return head.next;
    }
}
