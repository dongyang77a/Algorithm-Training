package 牛客101.链表;

import com.sun.org.apache.xalan.internal.xsltc.dom.CurrentNodeListFilter;

import java.util.Stack;

public class 反转链表 {


    public ListNode ReverseList (ListNode head) {
        // write code here
        ListNode prev = null;
        ListNode current = head;
       while(current != null)
       {
           ListNode nextTemp = current.next;
           current.next = prev;
           prev = current;
           current = nextTemp;
       }
       return prev;
    }
}
