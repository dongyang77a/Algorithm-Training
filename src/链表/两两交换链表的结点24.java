package 链表;
// 这个需要大胆赋值，将prev 做为空，将cur 与 next 进行交换，然后规定好指针的方向
// 继续来移到到下一个该交换的链表点位即可，注意交换顺序不能混乱
public class 两两交换链表的结点24 {
  static class   ListNode{
      int val;
      ListNode prev;
      ListNode next;
      ListNode(){}
      ListNode(int val){
          this.val=val;
      }
      ListNode(int val,ListNode next){
          this.val = val;
          this.next = next;
      }

    }
    public ListNode head;
    public ListNode last;
    public ListNode swapPairs(ListNode head){
        if(head == null || head.next == null){
            return head;
        }
        ListNode newhead = new ListNode(0);
        newhead.next = head;
        ListNode prev = newhead,cur = prev.next,next = cur.next,nnext = next.next;
        while(cur!=null && next!=null)
        {
            //交换节点
            prev.next = next;
            next.next = cur;
            cur.next = nnext;

            //修改指针
            prev = cur; // 注意顺序
            cur = nnext;
            if(cur!=null) next = cur.next;
            if(next != null) nnext = next.next;

        }
        return newhead.next;
    }
}
