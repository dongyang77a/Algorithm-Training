package 链表;
// 模拟两数相加的过程
public class 链表相加2 {

    static  class ListNode{
        public int val;
        public ListNode prev ; // 前驱
        public ListNode next; // 后继
        public ListNode(int val){
            this.val = val;
        }
    }
    public ListNode head;
    public ListNode last;

    public ListNode addTwoNumbers(ListNode l1,ListNode l2)
    {
        ListNode cur1 = l1;
        ListNode cur2 = l2;
        ListNode newHead = new ListNode(0);
        ListNode prev = newHead;// 尾插操作的尾指针
        int t = 0; //记录进位

        while(cur1 != null || cur2!=null || t!=0)
        {
            //先加第一个
            if(cur1 != null)
            {
                t+=cur1.val;
                cur1 = cur1.next;
            }
            if(cur2!=null)
            {
                t+=cur2.val;
                cur2 = cur2.next;
            }
            prev.next = new ListNode(t%10);
            prev = prev.next;
            t/=10;
        }
        return newHead.next;
    }
}
