package Test;
/*力扣算法*/
import java.util.Arrays;

public class Solution {
    public static int triangleNumber(int[] nums) {

        Arrays.sort(nums);
        int n=nums.length, ret=0;
        for(int i=n-1;i>=2;i--){
            int right=i-1;
            int left=0;
            while(left<right){
                if(nums[right]+nums[left]>nums[i]){
                    ret+=right-left;
                    right--;
                }
                else{
                    left++;
                }
            }
        }
        return ret;
    }

    public static void main(String[] args) {
        int []nums={2,2,3,4};
        int e=triangleNumber(nums);
        System.out.println(e);
    }
}
