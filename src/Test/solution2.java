package Test;

import java.util.ArrayList;
import java.util.List;

public class solution2 {

    public List<Character>  find (String ss, String pp){
        List<Character>  list = new ArrayList<Character>();
        char [] p = pp.toCharArray();
        char [] s = ss.toCharArray();
        int [] hash = new int[128];
        for( char x : p)  hash[x]++;
        for(int z = 0 ; z<s.length; z++){
            char in = s[z];
            if(hash[in] <1){
                list.add(in);
            }
        }
        return list;
    }

    public static void main(String[] args) {
        String ss = "They are student";
        String pp = "aeiou";
        solution2 a= new solution2();
        List<Character>list1 =a.find(ss,pp);
        StringBuffer n = new StringBuffer();
        for(Character zz :list1){
            n.append(zz);
        }
         System.out.println( n );
    }
}
