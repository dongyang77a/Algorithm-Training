package 算法强训.day5;

import java.util.Scanner;

public class 游游的you {

    public static int findMax(int a, int b, int c){
        int min = Math.min(a,Math.min(b,c));
        int result = 0;
        if(min == a || min == b){
            result += min*2+ b-min;
        }else{
            result += b*2;
        }

        return result;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        int i = 0;
        while(i<=t){
            int a = in.nextInt();
            int b = in.nextInt();
            int c =in.nextInt();
            System.out.println(findMax(a,b,c));
            i++;
        }


    }
}
