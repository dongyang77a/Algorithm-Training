package 算法强训.day2;

import java.util.Scanner;

public class 快递寄件问题 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double size = in.nextDouble();
        String status = in.next();

        int price = 20; // 起步价

        // 计算超出1kg的重量，不足1kg按1kg计算
        int extraKg = (int)Math.ceil(size - 1);
        price += extraKg;

        // 加急费用
        if ("y".equals(status)) {
            price += 5;
        }

        System.out.println(price);
    }


}
