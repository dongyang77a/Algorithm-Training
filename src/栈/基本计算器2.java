package 栈;

import java.util.ArrayDeque;
import java.util.Deque;
/*思路：先创建一个栈，然后把字符数组化，逐一检索，当检索一个完整字符时逐渐处理他们的位数与符号，
把他们拆分一个一个入栈，以计算符号为拆分分割，最后出栈一个一个加完即可*/
public class 基本计算器2 {

    public int caculate(String ss) {
        Deque <Integer> st = new ArrayDeque<>();
        char op = '+';
        int i = 0 , n = ss.length();
        char [] s = ss.toCharArray();
        while(i< n)
        {
            if(s[i] == ' ') i++;  // 如果为空格，就跳过
            else if( s[i]>='0'&&  s[i]<='9') // 为数字时
            {
                int tmp = 0 ;
                while(i<n && s[i]>='0'&&  s[i]<='9')// 循环读取字符，当遇到符号位时跳出循环
                {
                    tmp = tmp*10+ (s[i]-'0');
                    i++;
                }
                if(op == '+') st.push(tmp);
                else if(op == '-') st.push(-tmp);
                else if (op == '*') st.push(st.pop()* tmp);
                else st.push(st.pop()/ tmp);
            }
            else  // 如果是运算符
            {
                op = s[i]; // 更新运算符
                i++;
            }
        }
        // 统计结果
        int ret = 0;
        while(!st.isEmpty()){
            ret+=st.pop();
        }

        return ret;
    }
}
