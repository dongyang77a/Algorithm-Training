package 栈;

public class 比较含退格的字符串 {
    public boolean solution(String s,String t){
        return changeStr(s).equals(changeStr(t));
    }
    public String changeStr(String s)
    {
        StringBuffer ret = new StringBuffer();//用数组模拟栈结构
        for(int i  = 0; i<s.length();i++){
            char ch = s.charAt(i);
            if(ch != '#')
            {
                ret.append(ch); // 入栈
            }else
            {
                if(ret.length()>0) ret.deleteCharAt(ret.length()-1);// 出栈
            }
        }
        return ret.toString();
    }
}
