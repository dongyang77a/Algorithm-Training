package 栈;
// 给出一个由小写字母S，重复项删除操作会选择两个相邻且项目的字母
// 并删除他们，在S上反复进行删除操作，直到无法删除

public class 删除字符串中相邻重复项 {

    public String removeDuplicates(String ss)
    {
        StringBuffer ret = new StringBuffer();
        char [] s = ss.toCharArray();
        for(char ch :s){
            if(ret.length() > 0 && ch == ret.charAt(ret.length())-1){
                // 出栈
                ret.deleteCharAt(ret.length()-1);
            }else{
                // 进栈
                ret.append(ch);
            }
        }

        return ret.toString();
    }
}
