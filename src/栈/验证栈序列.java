package 栈;

import java.util.Stack;
// 思路设定两个栈，根据他的顺序进行验证操作。如果最终两个指针都相同则为 true 否则遍历完不同为false
public class 验证栈序列 {
    public boolean validateStackSequences(int[] pushed, int[] popped) {
        Stack<Integer> st = new Stack<>();
        int i = 0 , n = popped.length;
        for(int x : pushed)
        {
            st.push(x);
            while(!st.isEmpty() && st.peek() == popped[i])
            {
                st.pop();
                i++;
            }
        }
        return i == n;
    }
}
